//
//  ViewController.swift
//  KistanovLions
//
//  Created by WSR on 6/23/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON

struct place {
    var title: String
    var desc: String
    var coord: CLLocationCoordinate2D
    var imgName: String
}


class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let imgName: String
   
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, imgName: String) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.imgName = imgName
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.showsUserLocation = true
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.delegate = self
            let location = mapView.userLocation
            location.title = "Вы тут"
            loadArts()
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        let radius = CLLocationDistance(10000)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), latitudinalMeters: radius, longitudinalMeters: radius)
        self.mapView.setRegion(region, animated: false)
    }
    
    func loadArts(){
        let url = "http://cars.areas.su/arts"
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for item in json.arrayValue {
                    let aw = Artwork(title: item["title"].stringValue, locationName: item["subTitle"].stringValue, coordinate: CLLocationCoordinate2D(latitude: item["lat"].doubleValue, longitude: item["long"].doubleValue), imgName: item["image"].stringValue)
                    self.mapView.addAnnotation( aw )
                }
                case .failure(let error):
                print(error)
            }
        }
    }
    

    
        internal func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard let annotation = annotation as? Artwork else { return nil }
            let identifier = "marker"
            var view: MKMarkerAnnotationView
            
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {

                view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.glyphText = "📌"
                view.markerTintColor = .white
                view.canShowCallout = true
                view.isDraggable = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                let mapsButton = UIButton(  frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 200, height: 200)))
                let urlStr = annotation.imgName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                
                if let url = URL(string: urlStr!) {
                    if let data = try? Data(contentsOf: url){
                        mapsButton.setBackgroundImage( UIImage(data: data), for: UIControl.State())
                    }
                }
                
                view.rightCalloutAccessoryView = mapsButton
                let detailLabel = UILabel()
                detailLabel.numberOfLines = 0
                detailLabel.font = detailLabel.font.withSize(12)
                detailLabel.text = annotation.subtitle
                detailLabel.heightAnchor.constraint(equalToConstant: 200).isActive = true
                view.detailCalloutAccessoryView = detailLabel
            }
            return view
            
        }
   
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork

        self.mapView.removeOverlays(self.mapView.overlays)
        
        let srcCoord = mapView.userLocation.coordinate,
        targetCoord = location.coordinate
        
        let src = MKPlacemark(coordinate: srcCoord),
        target = MKPlacemark(coordinate: targetCoord)
        
        let req = MKDirections.Request()
        
        req.source = MKMapItem(placemark: src)
        req.destination = MKMapItem(placemark: target)
        req.transportType = .walking
        
        let direction = MKDirections(request: req)
        
        direction.calculate{
            (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    print("we have error getting directions==\(error.localizedDescription)")
                }
                return
            }
            
            let route = directionResonse.routes[0]
            self.mapView.addOverlay(route.polyline, level: .aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay)
        render.strokeColor = UIColor.blue
        render.lineWidth = 4.0
        return render
        
    }
    

}


